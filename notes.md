### Requirements
Install the thumbv7em-none-eabihf target: Run rustup target add thumbv7em-none-eabihf.
Install ST-Link: sudo pacman -S stlink openocd gdb arm-none-eabi-gdb

### Running on HW
Start debug connection: sudo openocd -f board/stm32f7discovery.cfg
Call cargo run -> does not work

build with cargo build --release
arm-none-eabi-objcopy -O binary target/thumbv7em-none-eabihf/release/stm32f7_test target/thumbv7em-none-eabihf/release/stm32f7_test.bin
st-flash write target/thumbv7em-none-eabihf/release/stm32f7_test.bin 0x8000000

### Links
* https://github.com/embed-rs/stm32f7-discovery

### Errata
2019-04-12 currently there is a problem compiling /core/src/lib.rs with the current nightly 2019-04-11)

rustup install nightly-2019-03-25
rustup override set  nightly-2019-03-25