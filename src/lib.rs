#![no_std]
#![feature(trusted_len)]
#![feature(optin_builtin_traits)]
#![feature(generator_trait)]
#![feature(arbitrary_self_types)]
#![feature(drain_filter)]
#![feature(never_type)]
#![feature(generators)]
#![feature(async_await)]
#![feature(const_transmute)]
#![feature(alloc_prelude)]
#![warn(missing_docs)]

#![feature(alloc)]
extern crate alloc;
extern crate cortex_m_rt as rt;

#[macro_use]
pub mod gpio;
pub mod init;
//pub mod interrupts;
pub mod system_clock;